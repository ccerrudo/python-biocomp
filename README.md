# Programación en Python para Biología Computacional

## Universidad Nacional de Quilmes - Octubre 2019

Material de trabajo para el curso de posgrado "Programación en Python para Biología Computacional".

Dictado en la Universidad Nacional de Quilmes

* Sede: Roque Sáenz Peña 352 (B1876BXD), Bernal, Buenos Aires, Argentina.
* Período: Desde el lunes 7 de octubre al viernes 11 de octubre de 2019.
* Cursado: Teoría-práctica de 13 a 18 hs. todos los días y de 9 a 13 hs. el martes 8 de octubre. 
Práctica supervisada de miércoles a viernes de 9 a 13 hs.
Ayuda para configuración inicial el lunes 7 de octubre de 10 a 13 hs.
Ver [cronograma detallado](General/cronograma.md).
* Aulas: Según [esquema de aulas asignadas](General/aulas.md) (ver [mapa de aulas](General/map_progpythonbiocomp.png))
###   Links Útiles:
* :cd: [Instalar Programas Necesarios](https://docs.google.com/document/d/1dDoHwmoXo6r9oUvqwOBYkYJ_JbufQnjw-aQv59CbyN8/edit?usp=sharing)
___
## Contenidos del curso
* #### [Clase I.](https://gitlab.com/npalopoli/python-biocomp/tree/master/Clase_I) Bioinformática, la terminal de Unix y control de versiones con Git
----
* #### [Clase II.](https://gitlab.com/npalopoli/python-biocomp/tree/master/Clase_II) Python: Variables y control de flujo
----
* #### [Clase III.](https://gitlab.com/npalopoli/python-biocomp/tree/master/Clase_III) Python: Funciones, archivos y errores
----
* #### [Clase IV.](https://gitlab.com/npalopoli/python-biocomp/tree/master/Clase_IV) Python: Manejo de datos
----
* #### [Clase V.](https://gitlab.com/npalopoli/python-biocomp/tree/master/Clase_V) Python: Visualización de datos
----
* #### [Clase VI.](https://gitlab.com/npalopoli/python-biocomp/tree/master/Clase_VI) Python para Biología

----
* #### :books: Bibliografía optativa 
 * [VV. AA. Software Carpentry: La Terminal de Unix.](https://swcarpentry.github.io/shell-novice-es/)
 * [VV. AA. Software Carpentry: El Control de Versiones con Git.](https://swcarpentry.github.io/git-novice-es/)
 * [Ekmekci B, McAnany CE, Mura C. An Introduction to Programming for Bioscientists: A Python-Based Primer. PLoS Comput Biol. 2016 Jun 7;12(6):e1004867.](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004867)
 * [Bahit E. Curso: Python para Principiantes.](https://www.iaa.csic.es/python/curso-python-para-principiantes.pdf)
 * [Jones M. Python for Biologists.](http://pythonforbiologists.com/)
 * [White E. Programming for Biologists.](http://www.programmingforbiologists.org/material/)
 * [Pratusevich M. Practice Python.](http://www.practicepython.org/)
 * [Kirienko D. et al. Snakify.](http://snakify.org/)
 * [Müller M. Scientific Plotting with Matplotlib.](http://www.python-academy.com/download/pycon2012/matplotlib_handout.pdf)
 * [Varios autores. Matplotlib Documentation.](http://matplotlib.org)
 * [Varios autores. Seaborn Documentation.](http://seaborn.pydata.org)
 * [McKinney W, PyData Development Team. Pandas: powerful Python data analysis toolkit.](https://pandas.pydata.org/pandas-docs/stable/pandas.pdf)
 * [Varios autores. Pandas Documentation.](https://pandas.pydata.org/pandas-docs/stable/)
 * [Varios autores. Biopython Documentation.](http://biopython.org/wiki/Documentation)
 * [Varios autores. Biopython Tutorial and cookbook.](http://biopython.org/DIST/docs/tutorial/Tutorial.pdf)
 * [Vos R. Bio::Phylo Documentation.](https://informatics.nescent.org/w/images/5/57/BioPhylo.pdf)
 * [Huerta-Cepas J, Serra F, Bork P. ETE Toollkit.](http://etetoolkit.org/)
 * [Varios Autores. Lasagne documentation.](https://media.readthedocs.org/pdf/lasagne/latest/lasagne.pdf)
 * [Varios autores. Python Enhancement Proposals (PEPs).](https://www.python.org/dev/peps/)

